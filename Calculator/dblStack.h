#pragma once

class dblStack {

private:
    int arIndex;
public:
    double ar[1000];

    dblStack::dblStack(void) {
    }

    dblStack::~dblStack(void) {
    }

    void dblStack::init() {
        arIndex = -1;
    }

    void dblStack::push(double c) {
        arIndex++;
        ar[arIndex] = c;
    }

    double dblStack::pop() {
        arIndex--;
        return ar[arIndex+1];
    }

    int dblStack::index() {
        return arIndex;
    }
};

