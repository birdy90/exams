#pragma once
#include <math.h>
#include "strStack.h"
#include "dblStack.h"
#include <cmath>

class calculator {
	static int priority(char c) 
    {
        if (c=='(') { return 0; }
        if (c=='+' || c=='-') { return 1; }
        if (c=='*' || c=='/') { return 2; }
        if (c=='^' || c=='s' || c=='c' || c=='t') { return 3; }
    }

	static double mypow(double a, double b)
	{
		if(a < 0)
		{
			std::complex<double> powres = std::pow(std::complex<double>(a), std::complex<double>(b)); 
			double len = sqrt(powres.real() * powres.real() + powres.imag() * powres.imag());
			len *= powres.real() > 0 ? -1 : 1; 
			return len;
		}
		else 
			return pow(a,b); 
	}

public:
	static double solve(char eq[], double x, double p = 0)
	{
		/*��������������� �������� ���������*/
        strStack sstack;
        sstack.init();
        char tempStr[1000] = "";
        int j = 0;
        for (int i = 0; i<(int) strlen(eq) ; i++) { /*�� ����� ������, continue ��� ����, ����� �� ���������� ��� �������*/
            if (eq[i]==' ') { /*������� ����������*/
                continue;
            }
            if (eq[i]=='(') { /*����������� ������ ������� � ����*/
                sstack.push(eq[i]);
                continue;
            }
            if (eq[i]==')') { /*���� ����������� ������, �� ������� �� ����� ��� �� ����������� ������*/
                while (sstack.top() != '(') {
                    tempStr[j]=sstack.pop();
                    j++;
                }
                sstack.pop(); /*���� ����������� ������ ������� �� �����*/
                continue;
            }
            if ( (eq[i] >= '0' && eq[i] <= '9') ) { /*����� ���������� � ������ ����������*/
                tempStr[j]='('; j++;
                while ( (eq[i] >= '0' && eq[i] <= '9') || eq[i] == '.' ) {
                    tempStr[j]=eq[i];
                    j++;
                    i++;
                }
                tempStr[j]=')'; 
                j++;
                i--; //����� �� "������ �� ������" ����� ������������� ����� ��������
                continue;
            }
            if (eq[i] == 'x') {
                tempStr[j] = 'x';
                j++;
                continue;
            }
            if (eq[i] == 'p') {
                tempStr[j] = 'p';
                j++;
                continue;
            }
            if ( priority(eq[i]) > priority(sstack.top()) || sstack.index() < 0 ) { /*���� ��������� ���� ��� ���� ������*/
                sstack.push(eq[i]); 
                if (eq[i]=='s' || eq[i]=='c' || eq[i]=='t')
                    i+=2;
                continue;
            }
            while ( priority(eq[i]) <= priority(sstack.top()) && sstack.index()>=0 && sstack.top() != '(') { /*���� ��������� ���� ��� ����� ��*/
                tempStr[j]=sstack.pop();
                j++;
            }
            sstack.push(eq[i]); 
            continue;   
        }
        while ( sstack.index() >= 0) { /*������� ����*/
            tempStr[j]=sstack.pop();
            j++;
        } 
        /*������� ���������*/
        dblStack dstack;
        dstack.init();
        for (int i=0; i <= j; i++)
        {
            if (tempStr[i]>='0' && tempStr[i]<='9'){ /*����� ����� ���������*/
                double a=atof(&tempStr[i]);
                i++;
                while ( tempStr[i] != ')') 
                    i++;
                dstack.push(a);
                continue;
            }
            switch (tempStr[i]) { /*������������ �����*/
            case 'x': { 
                dstack.push(x);
                continue;
                      }
            case 'p': { 
                dstack.push(p);
                continue;
                      }
            case '+': { 
                double a1 = dstack.pop();
                double a2 = dstack.pop();
                dstack.push(a1 + a2);
                continue;
                      }
            case '-': {
                double a1 = dstack.pop();
                double a2 = dstack.pop();
                dstack.push(a2 - a1);
                continue;
                      }
            case '*': {
                double a1 = dstack.pop();
                double a2 = dstack.pop();
                dstack.push(a1*a2);
                continue;
                      }
            case '/': {
                double znamenatel = dstack.pop();
                double chislitel = dstack.pop();
                dstack.push(chislitel/znamenatel);
                continue;
                      }
            case '^': {
                double pokazatel = dstack.pop();
                double osnovanie = dstack.pop();
                dstack.push(mypow(osnovanie, pokazatel));
				//dbstack.push(sign(osnovanie)*pow(abs(osnovanie),(pokazatel)));
				//dstack.push(exp(pokazatel*log(osnovanie)));
                continue;
                      }
            case 's': {
                double a = dstack.pop();
                dstack.push(sin(a));
                continue;
                      }
            case 'c': {
                double a = dstack.pop();
                dstack.push(cos(a));
                continue;
                      }
            case 't': {
                double a = dstack.pop();
                dstack.push(tan(a));
                continue;
                      }
            }
        }
        return dstack.ar[0];
    }
};