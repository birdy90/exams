#pragma once

class strStack {

private:
    char st[1000];
    int stIndex;
public:
    strStack::strStack(void) {
    }

    strStack::~strStack(void) {
    }

    void strStack::init() {
        stIndex = -1;
        strcpy(st, "");
    }

    void strStack::push(char c) {
        stIndex++;
        st[stIndex] = c;
    }

    char strStack::pop() {
        stIndex--;
        return st[stIndex+1];
    }

    char strStack::top() {
        return st[stIndex];
    }

    int strStack::index() {
        return stIndex;
    }
};

