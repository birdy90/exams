#include <iostream>
#include "Eigen"
#include "Extensions"

using namespace Eigen;
using namespace std;

//////////////////////////////////////////////////////////////////////////
// 6. ������ ������� ������� ����������� ������� ���� 
// ��� ��������� ������ ����������� ������� ������
//////////////////////////////////////////////////////////////////////////
void main()
{
	setlocale(LC_ALL, "Russian");

	// ��������� A � b �� ������
	MatrixXd A = FromFile("A.m");	

	// ������������� ����������� ������� ������
	// 1�� ����� ������� - ������������� ����������� ��� A
	// 2�� ����� ������� - ������������� ����������� ��� b
	MatrixXd s = FromFile("s.m");

	// ������ http://www.exponenta.ru/educat/class/courses/vvm/theme_4/theory.asp
	// �������� ����������: http://www.youtube.com/watch?v=Kult0wRkBG8
	// ������ MathCad http://www.exponenta.ru/educat/class/courses/vvm/theme_4/mathcad/ex3/ex3.htm

	// ���� ������������� ����� ��������������� �������
	// ����� ��������������� ����� �������� �� ������� ���
	// ����� �������� ����������� �������	
	// ����� norm() ���������� ��������� �����
	double condA = A.norm() * A.inverse().norm();		

	// ������������� ������ �����������
	// ������� http://www.exponenta.ru/educat/class/courses/vvm/theme_4/images/image075.gif
	double eps = (s(0) + s(1)) * condA;

	cout << "������� �: \n" << A << endl << endl;
	cout << "����������� �: " << s(0) << endl;
	cout << "����������� b: " << s(1) << endl;
	cout << "����� ��������������� �������: " << condA << endl;
	cout << "������������� ������ ����������� �������: " << eps << endl;

	getchar();
}