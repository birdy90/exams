#include <iostream>
#include "Dense"
#include "Extensions"

using namespace Eigen;
using namespace std;

#define PI 3.141592653589793238462643383279
#define EPS 0.000001

// ����� ����� ��� ���������� �����������
// �������� � ��������
// �������� http://catstail.narod.ru/jacobi.html
void Jacobi(MatrixXd A)
{	
	int iterations = 0;
	int n = A.cols();	

	MatrixXd eigenvecs = MatrixXd::Identity(n, n);	

	for(; iterations < 1000; iterations++)
	{	
		int maxi = 0;
		int maxj = 1;
		double Amax = pow(A(maxi, maxj),2);		
		for (int i = 0; i < n; i++)	
			for (int j = i + 1; j < n; j++)			
				if(pow( A(i,j) , 2 ) > Amax)
				{
					Amax = pow( A(i,j) , 2 );
					maxi = i;
					maxj = j;				
				}			
		
		if(Amax < EPS) break;
		double jcos, jsin;
		if(abs(A(maxi,maxi) - A(maxj,maxj)) > EPS)
		{
			double p = 2 * A(maxi,maxj) / (A(maxi,maxi) - A(maxj,maxj));
			jcos = sqrt(0.5 * (1 + 1 / sqrt(1 + p * p)));
			jsin = (abs(p) / p) * sqrt(0.5 * (1 - 1 / sqrt(1 + p * p)));
		}
		else
		{
			jcos = cos(PI / 2);
			jsin = sin(PI / 2);
		}			

		MatrixXd T = MatrixXd::Identity(n, n);
		T(maxi,maxi) = jcos;	
		T(maxi,maxj) = jsin;
		T(maxj,maxi) = -jsin;
		T(maxj,maxj) = jcos;

		A = (T * A) * T.transpose();
		eigenvecs = eigenvecs * T.transpose();

		cout << "��������: " << iterations << endl;
	}	
	
	cout << "��������� ��������:\n" << A << endl << endl;
	cout << "����������� ��������:\n" << A.diagonal() << endl << endl;
	cout << "����������� �������:\n" << eigenvecs << endl << endl;	
}

//////////////////////////////////////////////////////////////////////////
// 7. ���������� ����������� �������� � ����������� �������� ������������ ������
//////////////////////////////////////////////////////////////////////////
void main()
{
	setlocale(LC_ALL, "Russian");

	// ������ ������� �� �����
	MatrixXd A = FromFile("A.m");

	// ������������ �� Eigen Solver 
	// http://eigen.tuxfamily.org/dox/classEigen_1_1EigenSolver.html
	EigenSolver<MatrixXd> es(A);	

	// ������:
	// ��� ������������ ������ ������������ ����� �����:
	// http://en.wikipedia.org/wiki/Jacobi_eigenvalue_algorithm
	// http://ru.wikipedia.org/wiki/�����_�����_���_�����������_��������
	// http://catstail.narod.ru/jacobi.html

	// � ����� ����:
	// http://webmath.exponenta.ru/s/pyartli1/node80.htm	

	// ������� ������� �������� �����
	Jacobi(A);

	// ������������ �������
	cout << "���������� �������:" << endl;
	cout << "����������� ��������:" << endl << es.eigenvalues() << endl << endl;
	cout << "����������� �������:" << endl << es.eigenvectors() << endl << endl;
	
	getchar();
}