
// 9_ApproximationProblemDlg.h : ���� ���������
//

#pragma once


// ���������� ���� CMy9_ApproximationProblemDlg
class CMy9_ApproximationProblemDlg : public CDialogEx
{
// ��������
public:
	CMy9_ApproximationProblemDlg(CWnd* pParent = NULL);	// ����������� �����������

// ������ ����������� ����
	enum { IDD = IDD_MY9_APPROXIMATIONPROBLEM_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	double leftX;
	double rightX;
	int pointsCount;
	CString equation;
	afx_msg void OnBnClickedButton1();
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
};
