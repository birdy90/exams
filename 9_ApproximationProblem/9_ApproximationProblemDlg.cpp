// 9_ApproximationProblemDlg.cpp : ���� ����������

#include "stdafx.h"
#include "9_ApproximationProblem.h"
#include "9_ApproximationProblemDlg.h"
#include "afxdialogex.h"
#include "Eigen"
#include "calculator.h"
#include "Extensions"
#include "chart.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace Eigen;

Chart* g;

// ���������� ���� CMy9_ApproximationProblemDlg
CMy9_ApproximationProblemDlg::CMy9_ApproximationProblemDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMy9_ApproximationProblemDlg::IDD, pParent)
	, leftX(-2)
	, rightX(2)
	, pointsCount(10)
	, equation(_T("sin(x)"))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMy9_ApproximationProblemDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT6, leftX);
	DDX_Text(pDX, IDC_EDIT7, rightX);
	DDX_Text(pDX, IDC_EDIT4, pointsCount);
	DDX_Text(pDX, IDC_EDIT3, equation);
}

BEGIN_MESSAGE_MAP(CMy9_ApproximationProblemDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CMy9_ApproximationProblemDlg::OnBnClickedButton1)	
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()


// ����������� ��������� CMy9_ApproximationProblemDlg

BOOL CMy9_ApproximationProblemDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// ������ ������ ��� ����� ����������� ����. ����� ������ ��� �������������,
	//  ���� ������� ���� ���������� �� �������� ����������
	SetIcon(m_hIcon, TRUE);			// ������� ������
	SetIcon(m_hIcon, FALSE);		// ������ ������

	RECT win_rect;
	GetClientRect(&win_rect);
	win_rect.right -= 100;
	g = new Chart(win_rect);
	g->Create(NULL, NULL, WS_VISIBLE | WS_BORDER, win_rect, this, 0);
	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

// ��� ���������� ������ ����������� � ���������� ���� ����� ��������������� ����������� ���� �����,
//  ����� ���������� ������. ��� ���������� MFC, ������������ ������ ���������� ��� �������������,
//  ��� ������������� ����������� ������� ��������.

void CMy9_ApproximationProblemDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // �������� ���������� ��� ���������

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// ������������ ������ �� ������ ����������� ��������������
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// ��������� ������
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}

	g->SetFocus();
	g->ReDraw();
}

// ������� �������� ��� ������� ��� ��������� ����������� ������� ��� �����������
//  ���������� ����.
HCURSOR CMy9_ApproximationProblemDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CMy9_ApproximationProblemDlg::OnBnClickedButton1()
{
	UpdateData();
	
	// �������� ������� �� ���� �����
	char* eq = equation.GetBuffer(equation.GetLength() + 1);

	// ������:
	// http://ru.wikipedia.org/wiki/��������_������������
	
	// ���
	double step = (double)(rightX - leftX) / (pointsCount - 1);	
	
	// ��������� ��������
	double x = leftX;
	
	// ������� ��� �����������
	MatrixXd exact(2, pointsCount);
	MatrixXd exact2(2, pointsCount - 1);
	MatrixXd interp(2, pointsCount - 1);
		
	// ������ ������� ������ ��������
	for(int i = 0; i < pointsCount; i++)
	{		
		// �������� �������� ����� ������� � ����� �
		double y = calculator::solve(eq, x);

		// ������
		exact(0, i) = x;
		exact(1, i) = y;	

		x += step;
	}

	// ����������� �������� ������������
	for(int i = 0; i < pointsCount - 1; i++)
	{		
		// �������� �������� �����, ����� ����� ������� ����� ����
		double x0 = exact(0, i);
		double y0 = exact(1, i);
		double x1 = exact(0, i + 1);
		double y1 = exact(1, i + 1);

		// xh - ������� ����� x0 � x1
		double xh = (x1 - x0) / 2 + x0;

		// ������� � � xh (�������������, ��������� �������� "�������")
		double y = y0 + (y1 - y0) * (xh - x0) / (x1 - x0);
		
		// ���������� ���������
		interp(0, i) = xh;
		interp(1, i) = y;

		// ���������� ��������� ������ �������� � ���� �����
		exact2(0, i) = xh;
		exact2(1, i) = calculator::solve(eq, xh);
	}

	// ��������� ���������� � ����
	ToFile("Exact.m", exact2.transpose());
	ToFile("Interp.m", interp.transpose());

	// ������
	g->Clear();
	g->AddSeries(interp, "������������", 2, RED, true);
	g->AddSeries(exact, "������", 2, BLUE, true, false);
	g->AddSeries(exact2, "������ (������.)", 2, CYAN, true, false);
	g->ReDraw();
}


BOOL CMy9_ApproximationProblemDlg::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	return CDialogEx::OnMouseWheel(nFlags, zDelta, pt);
}
