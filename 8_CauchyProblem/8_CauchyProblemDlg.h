
// 8_CauchyProblemDlg.h : ���� ���������
//

#pragma once


// ���������� ���� CMy8_CauchyProblemDlg
class CMy8_CauchyProblemDlg : public CDialogEx
{
// ��������
public:
	CMy8_CauchyProblemDlg(CWnd* pParent = NULL);	// ����������� �����������

// ������ ����������� ����
	enum { IDD = IDD_MY8_CAUCHYPROBLEM_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	double step;
	double y0;
	CString equation;
	afx_msg void OnBnClickedButton1();
	void GetStepValue(double x, double y);
	double leftX;
	double rightX;
	afx_msg void OnDestroy();
	CString exact_equation;
};
