
// 8_CauchyProblemDlg.cpp : ���� ����������
//

#include "stdafx.h"
#include "8_CauchyProblem.h"
#include "8_CauchyProblemDlg.h"
#include "afxdialogex.h"

#include "../Chart/Chart.h"
#include "Extensions"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// ������� ������ ���� ��� ����������������� ��������� ������� �������. 
// ������� ��������������, ��������� �������� � ��� �������� � �������. 
// ������������ ����������c��� ����� ����. 
// �������� ���������� ��������� ������� � ������. 
// ��������� ������� ������� ������� � ������������� �������.
// ������ - ���. 430
// ������ t ���������� �������� p

// http://examhack.narod.ru/2_1.htm - ��� �����
// http://examhack.narod.ru/3_1.htm - ���������� ����

Chart* g;

CMy8_CauchyProblemDlg::CMy8_CauchyProblemDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMy8_CauchyProblemDlg::IDD, pParent)
	, step(0.1)
	, y0(1)
	, equation(_T("x*(2.7182^(0-p*p))-2*p*x"))
	, leftX(0)
	, rightX(1)
	, exact_equation(_T("2.7182^(0-x^2+0.8862)"))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMy8_CauchyProblemDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT4, step);
	DDX_Text(pDX, IDC_EDIT3, y0);
	DDX_Text(pDX, IDC_EDIT5, equation);
	DDX_Text(pDX, IDC_EDIT6, leftX);
	DDX_Text(pDX, IDC_EDIT7, rightX);
	DDX_Text(pDX, IDC_EDIT8, exact_equation);
}

BEGIN_MESSAGE_MAP(CMy8_CauchyProblemDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CMy8_CauchyProblemDlg::OnBnClickedButton1)
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// ����������� ��������� CMy8_CauchyProblemDlg

BOOL CMy8_CauchyProblemDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// ������ ������ ��� ����� ����������� ����. ����� ������ ��� �������������,
	//  ���� ������� ���� ���������� �� �������� ����������
	SetIcon(m_hIcon, TRUE);			// ������� ������
	SetIcon(m_hIcon, FALSE);		// ������ ������

	RECT win_rect;
	GetClientRect(&win_rect);
	win_rect.right -= 100;
	g = new Chart(win_rect);
	g->Create(NULL, NULL, WS_VISIBLE, win_rect, this, 0);
	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

// ��� ���������� ������ ����������� � ���������� ���� ����� ��������������� ����������� ���� �����,
//  ����� ���������� ������. ��� ���������� MFC, ������������ ������ ���������� ��� �������������,
//  ��� ������������� ����������� ������� ��������.

void CMy8_CauchyProblemDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // �������� ���������� ��� ���������

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// ������������ ������ �� ������ ����������� ��������������
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// ��������� ������
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}

	g->ReDraw();
}

// ������� �������� ��� ������� ��� ��������� ����������� ������� ��� �����������
//  ���������� ����.
HCURSOR CMy8_CauchyProblemDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CMy8_CauchyProblemDlg::GetStepValue(double x, double y)
{
	double epsilon = 0.001;
	double difference = 1;
	char* s = new char[equation.GetLength() + 1];
	strcpy(s, equation);
	double y1, y2, newStep = step;
	while (difference > epsilon)
	{
		UpdateData();
		step = newStep;
		UpdateData(0);
		newStep = step / 2;
		y1 = y + step * calculator::solve(s, x, y);
		y2 = y + newStep * calculator::solve(s, x, y);
		y2 = y2 + newStep * calculator::solve(s, x + newStep, y2);
		difference = y2 - y1;
	}
}


void CMy8_CauchyProblemDlg::OnBnClickedButton1()
{
	UpdateData();
	char* s = equation.GetBuffer(equation.GetLength() + 1);
	char* es = exact_equation.GetBuffer(exact_equation.GetLength() + 1);

	// ��������� ������������ ��������
	MatrixXd m(3, (int)((rightX - leftX) / step) + 1 );
	double j = y0;
	double x = leftX;
	m(0, 0) = x;
	m(1, 0) = j;
	m(2, 0) = j;

	// ��������� ������ ��������
	MatrixXd em(2, (int)((rightX - leftX) / step) + 1);
	em(0, 0) = x;
	em(1, 0) = calculator::solve(es, x);

	for (int i = 1; i < m.cols(); i++)
	{
		GetStepValue(m(0, i - 1), m(1, i - 1));
		m.conservativeResize(3, i + (rightX - x) / step + 1);
		em.conservativeResize(2, i + (rightX - x) / step + 1);

		x += step;

		m(0, i) = x;
		m(1, i) = m(1, i - 1) + step * calculator::solve(s, m(0, i - 1), m(1, i - 1));
		m(2, i) = m(2, i - 1) + step * 
			(calculator::solve(s, m(0, i - 1), m(2, i - 1)) + 
			calculator::solve(s, m(0, i - 1), m(1, i))) / 2;

		em(0, i) = x;
		em(1, i) = calculator::solve(es, em(0, i));
	}

	ToFile("out.txt", m.transpose());
	g->Clear();
	g->AddSeries(m, "������������", 2, BLUE, true);
	g->AddSeries(em, "������", 2, RED, true);
	g->ReDraw();
}

void CMy8_CauchyProblemDlg::OnDestroy()
{
	CDialogEx::OnDestroy();
	delete g;
}

