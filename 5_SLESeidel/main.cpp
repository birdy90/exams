#include <iostream>
#include "Dense"
#include "Extensions"

using namespace Eigen;
using namespace std;

//////////////////////////////////////////////////////////////////////////
// 5. Решение СЛАУ методом Зейделя
//////////////////////////////////////////////////////////////////////////
void main()
{
	setlocale(LC_ALL, "Russian");

	// Метод Гаусса-Зейделя:
	// http://ru.wikipedia.org/wiki/Метод_Зейделя
	// http://en.wikipedia.org/wiki/Gauss-Seidel_method	

	// Считываем матрицу, столбец значений и начальное приближение
	MatrixXd A = FromFile("A.m");
	VectorXd b = FromFile("b.m");		
	VectorXd xlast = FromFile("x0.m");		

	// Создаем необходимые матрицы
	MatrixXd x = VectorXd(A.cols());
	MatrixXd L = MatrixXd(A.rows(), A.cols());
	MatrixXd U = MatrixXd(A.rows(), A.cols());

	// Раскладываем A на L и U (L - диагональ и все что ниже, U - все остальное)	
	for(int i = 0; i < A.rows(); i++)
		for(int j = 0; j < A.cols(); j++)
		{
			L(i,j) = i >= j ? A(i,j) : 0;
			U(i,j) = i < j ? A(i,j) : 0;			
		}

	// Вычисляем T и С, как коэффициенты для итерационного алгоритма
	// x(k) = T * x(k-1) + C
	MatrixXd T = L.inverse() * U * -1;
	MatrixXd C = L.inverse() * b;	


	cout << "\nМатрица А:\n" << A << endl;
	cout << "\nВектор b:\n" << b << endl;
	cout << "\nНачальное приближение x0:\n" << xlast << endl;
	cout << "\nL:\n" << L << endl;
	cout << "\nU:\n" << U << endl;
	cout << "\nИтерация: x = T * xlast + C" << endl;

	double epsilon = 0.00001;

	// итерация
	x = T * xlast + C;		
	double k = log(epsilon / (A * x - b).norm()) / log(T.norm());
	int iterations = 0;

	cout << "Итераций гарантирующих сходимость: " << k << endl;

	do 
	{
		xlast = x;
		x = T * xlast + C;				
		iterations++;
	} while ((A * x - b).norm() >= epsilon);	

	// Выводим результаты
	cout << "\nРезультат решения СЛАУ x:\n" << x << endl;
	cout << "\nИтераций:\n" << iterations << endl;

	// Сохраняем в файл
	ToFile("x.m", x);

	getchar();
}