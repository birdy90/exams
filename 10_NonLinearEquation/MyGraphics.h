#pragma once

using namespace Eigen;

class MyGraphics : public CWnd
{
	DECLARE_DYNAMIC(MyGraphics)

public:
	MyGraphics(RECT r);
	virtual ~MyGraphics();

	CDC* dc;
	RECT rect;

	double left;
	double right;
	double unit;

	CPoint center;

	void Draw();
	void Draw(MatrixXd m);
	void Draw(MatrixXd m, MatrixXd exact);

	void PutPoint(double x, double y);
	void Axis();

protected:
	DECLARE_MESSAGE_MAP()
};


