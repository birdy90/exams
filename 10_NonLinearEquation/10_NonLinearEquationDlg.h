
// 10_NonLinearEquationDlg.h : ���� ���������
//

#pragma once
#include "afxwin.h"


// ���������� ���� CMy10_NonLinearEquationDlg
class CMy10_NonLinearEquationDlg : public CDialogEx
{
// ��������
public:
	CMy10_NonLinearEquationDlg(CWnd* pParent = NULL);	// ����������� �����������

// ������ ����������� ����
	enum { IDD = IDD_MY10_NONLINEAREQUATION_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnCbnSelchangeCombo1();
	int methodIndex;
	CComboBox methodSelect;
	CString equation;
	afx_msg void OnBnClickedButton1();
	double leftX;
	double rightX;
	CString stateText;
};
