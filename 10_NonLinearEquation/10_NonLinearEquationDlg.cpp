
// 10_NonLinearEquationDlg.cpp : ���� ����������
//

#include "stdafx.h"
#include "10_NonLinearEquation.h"
#include "10_NonLinearEquationDlg.h"
#include "afxdialogex.h"

#include "../Chart/chart.h"
#include "Extensions"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// ������� ����������� ��������� � �������� ��������� ��������� ������� 
// (������� �������� / ������� / ���������/ �������)
// ���������� �������.

Chart* g;
double epsilon = 0.0001;

CMy10_NonLinearEquationDlg::CMy10_NonLinearEquationDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMy10_NonLinearEquationDlg::IDD, pParent)
	, leftX(3)
	, rightX(4)
	, methodIndex(0)
	//, equation(_T("(2.7182^x)*(x^3)+2"))
	, equation(_T("(x^3)*(2.7182^x)+(x-1)^(1/3)+2"))
	, stateText(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMy10_NonLinearEquationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_CBIndex(pDX, IDC_COMBO1, methodIndex);
	DDX_Control(pDX, IDC_COMBO1, methodSelect);
	DDX_Text(pDX, IDC_EDIT5, equation);
	DDX_Text(pDX, IDC_EDIT6, leftX);
	DDX_Text(pDX, IDC_EDIT7, rightX);
	DDX_Text(pDX, IDC_STATIC8, stateText);
}

BEGIN_MESSAGE_MAP(CMy10_NonLinearEquationDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_CBN_SELCHANGE(IDC_COMBO1, &CMy10_NonLinearEquationDlg::OnCbnSelchangeCombo1)
	ON_BN_CLICKED(IDC_BUTTON1, &CMy10_NonLinearEquationDlg::OnBnClickedButton1)
END_MESSAGE_MAP()


// ����������� ��������� CMy10_NonLinearEquationDlg

void SetDropDownHeight(CComboBox* pMyComboBox, int itemsToShow)
{
	//Get rectangles
	CRect rctComboBox, rctDropDown;
	//Combo rect
	pMyComboBox->GetClientRect(&rctComboBox); 
	//DropDownList rect
	pMyComboBox->GetDroppedControlRect(&rctDropDown); 

	//Get Item height
	int itemHeight = pMyComboBox->GetItemHeight(-1); 
	//Converts coordinates
	pMyComboBox->GetParent()->ScreenToClient(&rctDropDown); 
	//Set height
	rctDropDown.bottom = rctDropDown.top + rctComboBox.Height() + itemHeight*itemsToShow; 
	//apply changes
	pMyComboBox->MoveWindow(&rctDropDown); 
}

BOOL CMy10_NonLinearEquationDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// ������ ������ ��� ����� ����������� ����. ����� ������ ��� �������������,
	//  ���� ������� ���� ���������� �� �������� ����������
	SetIcon(m_hIcon, TRUE);			// ������� ������
	SetIcon(m_hIcon, FALSE);		// ������ ������
	
	RECT win_rect;
	GetClientRect(&win_rect);
	win_rect.right -= 100;
	win_rect.bottom -= 50;
	g = new Chart(win_rect);
	g->Create(NULL, NULL, WS_VISIBLE, win_rect, this, 0);

	UpdateData(TRUE);
	methodSelect.AddString("����� ��������");
	methodSelect.AddString("����� �������");
	methodSelect.AddString("����� ���������");
	methodSelect.AddString("����� �������");
	SetDropDownHeight(&methodSelect, 4);
	methodSelect.SetCurSel(3);
	UpdateData(TRUE);

	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

// ��� ���������� ������ ����������� � ���������� ���� ����� ��������������� ����������� ���� �����,
//  ����� ���������� ������. ��� ���������� MFC, ������������ ������ ���������� ��� �������������,
//  ��� ������������� ����������� ������� ��������.

void CMy10_NonLinearEquationDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // �������� ���������� ��� ���������

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// ������������ ������ �� ������ ����������� ��������������
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// ��������� ������
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}

	g->ReDraw();
}

// ������� �������� ��� ������� ��� ��������� ����������� ������� ��� �����������
//  ���������� ����.
HCURSOR CMy10_NonLinearEquationDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CMy10_NonLinearEquationDlg::OnCbnSelchangeCombo1()
{
	// TODO: �������� ���� ��� ����������� �����������
}

double DerivativeAt(char* s, double x)
{
	double dx = 0.0001;
	return (calculator::solve(s, x + dx) - calculator::solve(s, x)) / dx;
}

double IterationsMethod(char* s, double x0)
{
	double res = calculator::solve(s, x0);
	int i = 0;
	do 
	{
		x0 = res;
		res = calculator::solve(s, res);
		i++;
		if (i > 1000)
		{
			throw(new exception("too long"));
		}
	} while (abs(res - x0) > epsilon);
	return res;
}

double NewtonMethod(char* s, double x0)
{
	double res = x0;
	int i = 0;
	do 
	{
		x0 = res;
		res = x0 - calculator::solve(s, x0) / DerivativeAt(s, x0);
		i++;
		if (i > 1000)
		{
			throw(new exception("too long"));
		}
	} while (abs(res - x0) > epsilon);
	return res;
}

double VegsteinMethod(char* s, double x0, double x1)
{
	// s: 0 = f(x)
	// es:  x = g(x)
	CString phi_equation = s;
	phi_equation += "+x";
	char* es = new char[phi_equation.GetLength() + 1];
	strcpy(es, phi_equation);

	int i = 0;
	double res;
	do 
	{
		res = x1 - (x1 - calculator::solve(es, x1)) / 
			(1 - (calculator::solve(es, x1) - calculator::solve(es, x0)) / (x1 - x0) );
		x0 = x1;
		x1 = res;
		i++;
		if (i > 1000)
		{
			throw(new exception("too long"));
		}
	} while (abs(res - x0) > epsilon);
	return res;
}

double SecantMethod(char* s, double x0, double x1)
{
	double res;
	int i = 0;
	do 
	{
		res = x1 - (x0 - x1) / (calculator::solve(s, x0) - calculator::solve(s, x1)) * calculator::solve(s, x1);
		x0 = x1;
		x1 = res;
		i++;
		if (i > 1000)
		{
			throw(new exception("too long"));
		}
	} while (abs(res - x0) > epsilon);
	return res;
}

void CMy10_NonLinearEquationDlg::OnBnClickedButton1()
{
	UpdateData(TRUE);
	char* s = equation.GetBuffer(equation.GetLength() + 1);
	int n = 1000;	
	double step = (double)20 / (n - 1);
	MatrixXd m(2, n);
	double x = -10;
	UpdateData(FALSE);
	for (int i = 0; i < n; i++)
	{
		m(0, i) = x;
		m(1, i) = calculator::solve(s, x);
		x += step;
	}
	g->Clear();
	g->AddSeries(m, "�������", 2, BLUE, false);
	ToFile("m.m", m);

	switch (methodIndex)
	{
	// ������, ���. 93
	case 0: // ����� ��������
		{
			// s: 0 = f(x)
			// es:  x = g(x)
			CString phi_equation = equation;
			phi_equation += "+x";
			char* es = new char[phi_equation.GetLength() + 1];
			strcpy(es, phi_equation);

			double t = abs(DerivativeAt(es, leftX));
			if (t <= 1)
			{
				try
				{
					UpdateData(TRUE);
					x = IterationsMethod(es, leftX);
					CString xs;
					xs.Format("%f", x);
					stateText = "x = " + xs;
					g->PutPoint(x, calculator::solve(s, x), "�������", 2, RED);
					UpdateData(FALSE);
				}
				catch (...)
				{
					UpdateData(TRUE);
					stateText = "����� ����������/";
					UpdateData(FALSE);
				}
			}
			else
			{
				UpdateData(TRUE);
				stateText = "����� ����������";
				UpdateData(FALSE);
			}
			break;
		}
	// ������, ���. 93
	// �����, ����� �����������
	case 1: // ����� �������
		{
			try
			{
				UpdateData(TRUE);
				x = NewtonMethod(s, leftX);
				CString xs;
				xs.Format("%f", x);
				stateText = "x = " + xs;
				g->PutPoint(x, calculator::solve(s, x), "�������", 2, RED);
				UpdateData(FALSE);
			}
			catch (...)
			{
				UpdateData(TRUE);
				stateText = "����� ����������. ��������� �����������";
				UpdateData(FALSE);
			}
			break;
		}
	// ����� ���������, ������ ������, �������� ������������ ������ �������, ������ ��� ����� ������� � ���������� ������� ������� ��������, ������������ ������������� �������
	// http://www.machinelearning.ru/wiki/index.php?title=%D0%9C%D0%B5%D1%82%D0%BE%D0%B4_%D0%BF%D1%80%D0%BE%D1%81%D1%82%D1%8B%D1%85_%D0%B8%D1%82%D0%B5%D1%80%D0%B0%D1%86%D0%B8%D0%B9#.D0.9C.D0.B5.D1.82.D0.BE.D0.B4_.D0.92.D0.B5.D0.B3.D1.81.D1.82.D0.B5.D0.B9.D0.BD.D0.B0
	case 2: // ����� ���������
		{
			try
			{
				UpdateData(TRUE);
				x = VegsteinMethod(s, leftX, rightX);
				CString xs;
				xs.Format("%f", x);
				stateText = "x = " + xs;
				g->PutPoint(x, calculator::solve(s, x), "�������", 2, RED);
				UpdateData(FALSE);
			}
			catch (...)
			{
				UpdateData(TRUE);
				stateText = "����� ����������";
				UpdateData(FALSE);
			}
			break;
		}
	// ������, ���. 114
	case 3: // ����� ������� (����)
		{
			try
			{
				UpdateData(TRUE);
				x = SecantMethod(s, leftX, rightX);
				CString xs;
				xs.Format("%f", x);
				stateText = "x = " + xs;
				g->PutPoint(x, calculator::solve(s, x), "�������", 2, RED);
				UpdateData(FALSE);
			}
			catch (...)
			{
				UpdateData(TRUE);
				stateText = "����� ����������. ��������� �����������";
				UpdateData(FALSE);
			}
			break;
		}
	}
	g->ReDraw();
}
