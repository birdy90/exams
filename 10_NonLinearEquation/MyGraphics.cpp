#include "stdafx.h"
#include "10_NonLinearEquation.h"
#include "MyGraphics.h"

IMPLEMENT_DYNAMIC(MyGraphics, CWnd)

MyGraphics::MyGraphics(RECT r)
{
	rect = r;
}

MyGraphics::~MyGraphics()
{
}

void MyGraphics::Axis()
{
	CPen pen = CPen(0, 1, RGB(150, 150, 150));
	dc->SelectObject(&pen);

	unit = rect.right / (right - left);
	center.x = rect.right / (right - left) * (- left);
	center.y = rect.bottom / 2;
	
	// x
	dc->MoveTo(center.x, 0);
	dc->LineTo(center.x, rect.bottom);
	// unit by x
	dc->MoveTo(center.x + unit, center.y);
	dc->LineTo(center.x + unit, center.y - 5);

	// y
	dc->MoveTo(0, center.y);
	dc->LineTo(rect.right, center.y);
	// unit by y
	dc->MoveTo(center.x, center.y - unit);
	dc->LineTo(center.x + 5, center.y - unit);
	DeleteObject(&pen);
}

void MyGraphics::Draw()
{
	dc->FillSolidRect(&rect, RGB(255, 255, 255));
	Axis();
}

void MyGraphics::Draw(MatrixXd m)
{
	Draw();

	CPen pen = CPen(0, 2, RGB(0, 0, 0));
	dc->SelectObject(&pen);

	for (int j = 1; j < m.rows(); j++)
	{
		dc->MoveTo(m(0, 0) * unit + center.x, 
			- m(j, 0) * unit + center.y);
		for (int i = 1; i < m.cols(); i++)
		{
			dc->LineTo(m(0, i) * unit + center.x, 
				- m(j, i) * unit + center.y);
		}
	}
	DeleteObject(&pen);
}

void MyGraphics::Draw(MatrixXd m, MatrixXd exact)
{
	Draw(m);

	CPen pen = CPen(0, 2, RGB(255, 0, 0));
	dc->SelectObject(&pen);
	dc->MoveTo(exact(0, 0) * unit + center.x, 
		- exact(1, 0) * unit + center.y);
	for (int i = 1; i < m.cols(); i++)
	{
		dc->LineTo(exact(0, i) * unit + center.x, 
			- exact(1, i) * unit + center.y);
	}
	DeleteObject(&pen);
}

void MyGraphics::PutPoint(double x, double y)
{
	CPen pen = CPen(0, 2, RGB(255, 0, 0));
	dc->SelectObject(&pen);
	dc->Ellipse(x * unit - 5 + center.x, y * unit - 5 + center.y,
		x * unit + 5 + center.x, y * unit + 5 + center.y);
	DeleteObject(&pen);
}

BEGIN_MESSAGE_MAP(MyGraphics, CWnd)
END_MESSAGE_MAP()