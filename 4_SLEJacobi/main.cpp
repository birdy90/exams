#include <iostream>
#include "Dense"
#include "Extensions"

using namespace Eigen;
using namespace std;

//////////////////////////////////////////////////////////////////////////
// 4. ������� ���� ������� ����� (������ ��������������� ������� ����� ��������, 
// �������������� ��������� ������� � �������� ���������)
//////////////////////////////////////////////////////////////////////////
void main()
{
	// ����� �����:
	// http://en.wikipedia.org/wiki/Jacobi_method
	
	// ������ ����������:
	// http://vm.psati.ru/online-vmath/index.php?page=10

	setlocale(LC_ALL, "Russian");

	// ��������� �������, ������� �������� � ��������� �����������
	MatrixXd A = FromFile("A.m");
	VectorXd b = FromFile("b.m");		
	VectorXd xlast = FromFile("x0.m");		

	// ������� ����������� �������
	MatrixXd x = VectorXd(A.cols());
	MatrixXd D = MatrixXd(A.rows(), A.cols());
	MatrixXd R = MatrixXd(A.rows(), A.cols());
	
	// ������������ A �� D � R (D - ���������, R - ��� ����� ��������� (R = L + U))
	for(int i = 0; i < A.rows(); i++)
		for(int j = 0; j < A.cols(); j++)
		{
			D(i,j) = i == j ? A(i,j) : 0;
			R(i,j) = i == j ? 0 : A(i,j);			
		}

	// ��������� T � �, ��� ������������ ��� ������������� ���������
	// x(k) = T * x(k-1) + C
	MatrixXd T = D.inverse() * R * -1;
	MatrixXd C = D.inverse() * b;	


	cout << "\n������� �:\n" << A << endl;
	cout << "\n������ b:\n" << b << endl;
	cout << "\n��������� ����������� x0:\n" << xlast << endl;
	cout << "\nD:\n" << D << endl;
	cout << "\nR:\n" << R << endl;
	cout << "\n��������: x = T * xlast + C" << endl;

	double epsilon = 0.00001;

	// ��������
	x = T * xlast + C;		
	double k = log(epsilon / (A * x - b).norm()) / log(T.norm());
	int iterations = 0;
	
	cout << "�������� ������������� ����������: " << k << endl;

	do 
	{
		xlast = x;
		x = T * xlast + C;				
		iterations++;
	} while ((A * x - b).norm() >= epsilon);	
	
	// ������� ����������
	cout << "\n��������� ������� ���� x:\n" << x << endl;
	cout << "\n��������:\n" << iterations << endl;

	// ��������� � ����
	ToFile("x.m", x);

	getchar();
}