#include "stdafx.h"
#include "Chart.h"

IMPLEMENT_DYNAMIC(Chart, CWnd)

Chart::Chart(RECT r)
{
 	Rect = r;
 	Zoom = 50;
 	PosX = 0;
 	PosY = 0;
	isDragging = false;
}

Chart::~Chart() {}

CPoint Chart::GetPosition(double x, double y)
{
 	CPoint res;
	res.x = (x - PosX)  * Zoom + (Rect.right - Rect.left)/2;
	res.y = (y - PosY)  * Zoom + (Rect.bottom - Rect.top)/2;
	res.y = Rect.bottom - Rect.top - res.y;
 	return res;
}	

CPoint Chart::GetAbsPosition(int x, int y)
{
	CPoint res;
	CPoint center = GetPosition(PosX,PosY);
	res.x = PosX - (float)(center.x - x) / Zoom;
	res.y = PosY + (float)(center.y - y) / Zoom;
	return res;
}
	
void Chart::AddSeries(MatrixXd m, char* name, int penWidth, COLORREF clr, bool drawPoints, bool drawLine)
{		
 	SeriesInfo si;
 	si.linePen = new CPen(0, penWidth, clr);
	si.name = name;
 	si.matrix = m;
	si.drawPoints = drawPoints;
	si.drawLine = drawLine;
 	series.push_back(si);
}

void Chart::PutPoint(double x, double y, char* name, int penWidth, COLORREF clr)
{
	SeriesInfo si;
 	si.linePen = new CPen(0, penWidth, clr);
	si.name = name;
 	si.matrix = MatrixXd(2,1);
 	si.matrix(0,0) = x;
 	si.matrix(1,0) = y;
	si.drawPoints = true;
 	series.push_back(si);
}

void Chart::Clear()
{
	series.clear();
}	

void Chart::ReDraw()
{		
	CDC	DC;
	CDC* dc = GetDC();
	int nDC = 0;
	CBitmap BmpDC;
	CBitmap *OldBmpDC;
	nDC = DC.CreateCompatibleDC(dc);
	if (nDC == 0){
		return;
	}

	BmpDC.CreateCompatibleBitmap(dc, Rect.right, Rect.bottom);
	OldBmpDC = DC.SelectObject(&BmpDC);
	DC.SetMapMode(MM_TEXT);
	DC.SetBkMode(TRANSPARENT);
	DC.SetTextColor(RGB(0,0,0));


 	DC.FillSolidRect(&Rect, BGCOLOR);	
 	DrawAxis(DC);	 	
	DrawSeries(DC);
	DrawLegend(DC);
	//DrawBounds(DC);

	dc->BitBlt(0, 0, Rect.right, Rect.bottom, &DC, 0, 0, SRCCOPY);

	DC.SelectObject(BmpDC);
	DC.DeleteDC();
	BmpDC.DeleteObject();
	ReleaseDC(&DC);
}

void Chart::DrawBounds(CDC& DC)
{
	CPen pen = CPen(0, 1, BLACK);
	DC.SelectObject(&pen);
	DC.MoveTo(Rect.left,Rect.top);
	DC.LineTo(Rect.right - 1,Rect.top);
	DC.LineTo(Rect.right - 1,Rect.bottom - 1);
	DC.LineTo(Rect.left,Rect.bottom - 1);
	DC.LineTo(Rect.left,Rect.top);
	DeleteObject(&pen);
}

void Chart::DrawAxis(CDC& DC) 
{
	CPen pen1(0, 1, BLACK);
	CPen pen2(0, 1, LGRAY);
	DC.SelectObject(&pen2);

	CPoint lt = GetAbsPosition(Rect.left, Rect.top);
	CPoint rb = GetAbsPosition(Rect.right,Rect.bottom);


	char buffer[20];
	CPoint p;

	for(int i = lt.x; i <= rb.x; i++)
	{
		p = GetPosition(i, 0);
		DC.MoveTo(p.x, Rect.top);
		DC.LineTo(p.x, Rect.bottom);
		DC.TextOutA(p.x + 3, Rect.bottom - 15, itoa(i, buffer, 10));
	}

	for(int i = lt.y; i >= rb.y; i--)
	{
		p = GetPosition(0, i);
		DC.MoveTo(Rect.left, p.y);
		DC.LineTo(Rect.right, p.y);
		DC.TextOutA(Rect.left + 3, p.y + 3, itoa(i, buffer, 10));
	}
	
	/*

	char buffer[20];

	for(int i = - 10; i <= 10; i++)
	{
		CPoint px = GetPosition(i, 0);
		CPoint py = GetPosition(0, i);

		DC.MoveTo(px.x, Rect.top);
		DC.LineTo(px.x, Rect.bottom);			
		DC.TextOutA(px.x + 3, Rect.bottom - 15, itoa(i, buffer, 10));

		DC.MoveTo(Rect.left, py.y);
		DC.LineTo(Rect.right, py.y);
		DC.TextOutA(Rect.left + 3, py.y + 3, itoa(i, buffer, 10));
	}
	*/

	DC.SelectObject(&pen1);
	CPoint center = GetPosition(0,0);
	DC.MoveTo(center.x, Rect.top);
	DC.LineTo(center.x, Rect.bottom);
	DC.MoveTo(Rect.left, center.y);
	DC.LineTo(Rect.right, center.y);

	DeleteObject(&pen1);
	DeleteObject(&pen2);

}

void Chart::DrawLegend(CDC& DC)
{
	CPen pen1(0, 1, BLACK);
	DC.SelectObject(pen1);
	if(series.size() > 0)
	{
		DC.Rectangle(0, 0, 150, series.size() * 20 + 10);
		CPoint offset;
		offset.x = 10;
		offset.y = 10;

		for(int i = 0; i < series.size(); i++)
		{
			SeriesInfo si = series[i];
			DC.SelectObject(si.linePen);
			DC.MoveTo(offset.x, offset.y + 2);
			DC.LineTo(offset.x + 15, offset.y + 2);
			if(si.drawPoints)
				DC.Ellipse(offset.x + 8 - POINTSIZE, offset.y + 2 -POINTSIZE, offset.x + 8 + POINTSIZE, offset.y + 2 + POINTSIZE);
			DC.TextOutA(offset.x + 20, offset.y - 5, si.name);
			offset.y += 20;
		}
	}
	DeleteObject(&pen1);
}

void Chart::DrawSeries(CDC& DC)
{
	// �������� �� �������� ������
	for(int i = 0; i < series.size(); i++)
	{			
		SeriesInfo current = series[i];
		if(current.matrix.rows() < 2) continue;
		if(current.matrix.cols() < 1) continue;

		DC.SelectObject(current.linePen);	
		CPoint p;

		// �������� �� �������
		for(int r = 1; r < current.matrix.rows(); r++)
		{  
			if(current.drawLine)
			{
				p = GetPosition(current.matrix(0, 0), current.matrix(r, 0));
				DC.MoveTo(p);

				for(int c = 1; c < current.matrix.cols(); c++)
				{
					p = GetPosition(current.matrix(0, c), current.matrix(r, c));
					DC.LineTo(p);
					DC.MoveTo(p);
				}
			}		

			if(current.drawPoints)
			{
				for(int c = 0; c < current.matrix.cols(); c++)
				{
					p = GetPosition(current.matrix(0, c), current.matrix(r, c));
					DC.Ellipse(p.x - POINTSIZE, p.y -POINTSIZE, p.x + POINTSIZE, p.y + POINTSIZE);
				}
			}
		}		
	}
}

BEGIN_MESSAGE_MAP(Chart, CWnd)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEWHEEL()
	ON_WM_MOUSEACTIVATE()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

void Chart::OnMouseMove(UINT nFlags, CPoint point)
{
	if(isDragging)
	{
		double dx = point.x - oldCurPos.x;
		double dy = point.y - oldCurPos.y;
		PosX -= dx / Zoom;
		PosY += dy / Zoom;
		ReDraw();		
	}

	oldCurPos = point;
	CWnd::OnMouseMove(nFlags, point);
}

void Chart::OnLButtonDown(UINT nFlags, CPoint point)
{
	isDragging = true;
	SetFocus();
	CWnd::OnLButtonDown(nFlags, point);
}

void Chart::OnLButtonUp(UINT nFlags, CPoint point)
{
	isDragging = false;
	CWnd::OnLButtonUp(nFlags, point);
}

BOOL Chart::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	if (zDelta == 0) return FALSE;
	float w = (Rect.right - Rect.left) / 2;
	float h = (Rect.bottom - Rect.top) / 2;
	float scaleRatio = 1.3f;		
	Zoom *= (float) pow(scaleRatio, abs(zDelta) / zDelta); // ���������� ��������� ��� ����������� �������
	
	Zoom = min(Zoom, 1500);
	Zoom = max(Zoom, 10);

	/*
	float mx = PosX + (pt.x - w) / Zoom;
	float my = PosY + ((2 * h - pt.y) - h) / Zoom;

	if (zDelta > 0) // �����������
	{
		PosX += (mx - PosX) * (scaleRatio - 1);
		PosY += (my - PosY) * (scaleRatio - 1);
	}
	else // ���������
	{
		PosX -= (mx - PosX) * (scaleRatio - 1) / scaleRatio;
		PosY -= (my - PosY) * (scaleRatio - 1) / scaleRatio;
	}
	*/
	ReDraw();
	return CWnd::OnMouseWheel(nFlags, zDelta, pt);
}

int Chart::OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message)
{
	SetFocus();
	isDragging = false;
	return CWnd::OnMouseActivate(pDesktopWnd, nHitTest, message);
}

BOOL Chart::OnEraseBkgnd(CDC* pDC)
{
	return TRUE;
}
