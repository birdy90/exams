#pragma once
#include "Eigen"
#include <vector>

class Chart;

using namespace Eigen;
using namespace std;

// Main Colors
#define BLACK 	RGB(0,0,0)
#define RED 	RGB(255,0,0) 
#define GREEN 	RGB(0,255,0) 
#define BLUE 	RGB(0,0,255) 
#define CYAN 	RGB(0,255,255) 
#define WHITE 	RGB(255,255,255) 
#define GRAY 	RGB(50,50,50) 
#define LGRAY 	RGB(230,230,230) 

#define BGCOLOR WHITE
#define POINTSIZE 4

struct SeriesInfo
{
	MatrixXd matrix;
	CPen* linePen;
	bool drawPoints;
	bool drawLine;
	char* name;
};

class Chart : public CWnd
{
	DECLARE_DYNAMIC(Chart)

public:
	Chart(RECT r);
	virtual ~Chart() ;
	RECT Rect;

	double PosX;	
	double PosY;
	double Zoom;

	CPoint GetPosition(double x, double y);	
	CPoint GetAbsPosition(int x, int y);
	void AddSeries(MatrixXd m, char* name, int penWidth, COLORREF clr, bool drawPoints, bool drawLine = true);
	void PutPoint(double x, double y, char* name, int penWidth, COLORREF clr);
	void Clear();
	void ReDraw();

private:
	vector<SeriesInfo> series;
	bool isDragging;
	CPoint oldCurPos;
	void DrawBounds(CDC& DC);
	void DrawAxis(CDC& DC);
	void DrawLegend(CDC& DC);
	void DrawSeries(CDC& DC);

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg int OnMouseActivate(CWnd* pDesktopWnd, UINT nHitTest, UINT message);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
};