#include "Dense"
#include "Extensions"

using namespace Eigen;

void main()
{
	// ��� ������� ���� � ���������� ��������� ������������ ����� ��������
	// ����� ���� ������ (����� 1, ����� 1.1, � ����� ������, ���������� ������) http://books.google.ru/books?id=1I31UX0cZt0C&pg=PA2&redir_esc=y#v=onepage&q&f=false
	MatrixXd A = FromFile("input.txt");
	VectorXd b = FromFile("input_b.txt");

	// ������ ��� ��������
	VectorXd al(A.rows());
	VectorXd be(A.rows());
	VectorXd ga(A.rows());
	VectorXd de(b);
	al(0) = 0;
	be(0) = A(0, 0);
	ga(0) = b(0) / A(0, 0);
	for (int i = 1; i < al.rows(); i++)
	{
		al(i) = A(i, i - 1);
		be(i) = A(i, i) - al(i) * A(i - 1, i) / be(i - 1);
		ga(i) = (b(i) - al(i) * ga(i - 1)) / be(i);
	}

	// �������� ��� ��������
	VectorXd x(A.cols());
	x(x.rows() - 1) = ga(x.rows() - 1);
	for (int i = x.rows() - 2; i >= 0; i--)
	{
		x(i) = ga(i) - A(i, i + 1) * x(i + 1) / be(i);
	}

	ToFile("output.txt", x);
}	