
#include <iostream>
#include "Dense"
#include "Extensions"

using namespace Eigen;
using namespace std;

/*void main() // �� ������
{
	// ��� ������� ���� � ����������� ������� �������
	// ������������ ����� ������ (��. ���. 137), ��� ���� � ������������ ���� ������� ���������� ��������
	// � ����������� � ���� �������� ������������ � �� ����� ������� �������
	MatrixXd A = FromFile("input.txt");
	MatrixXd Aprev(A); // �������� �������
	MatrixXd b = FromFile("input_b.txt");
	MatrixXd bprev(b); // �������� ������ �����

	// ������ ��� ������
	// �������� � � ������������ ����, ��������� ��������������� �������� � b
	for (int i = 0; i < A.rows(); i++)
	{
		for (int j = i + 1; j < A.rows(); j++)
		{
			double q = A(j, i) / A(i, i);
			A.row(j) -= A.row(i) * q;
			b.row(j) -= b.row(i) * q;
		}
		// �� ������� ��������� ��������� ��������
		b.row(i) /= A(i, i);
		A.row(i) /= A(i, i);
	}

	// �������� ��� ������
	// ��������� �
	MatrixXd x(A.cols(), b.cols());
	x.setZero();
	for (int i = A.cols() - 1; i >= 0; i--)
	{
		for (int j = 0; j < b.cols(); j++)
		{
			x(i, j) = b(i, j);
			for (int k = i + 1; k < A.cols(); k++)
			{
				x(i, j) -= x(k, j) * A(i, k);
			}
		}
	}

	ToFile("output.txt", x);
}*/

//////////////////////////////////////////////////////////////////////////
// 3. ������� ���� � ����������� ������� �������.
//////////////////////////////////////////////////////////////////////////
void main()
{
	setlocale(LC_ALL, "Russian");

	// ���������  �������� �������, 
	// � ������� ������ ������
	MatrixXd A = FromFile("A.m");
	MatrixXd b = FromFile("b.m");		
	MatrixXd x(b.rows(),b.cols());
	
	// ����������� �������
	MatrixXd Ainv = A.inverse();
	
	// ����� 1 - "� ���"
	// ������ xi = A^-1 * bi ��� ������ ������ �����	
	//for(int i = 0; i < b.cols(); i++)
		//x.col(i) = Ainv * b.col(i);

	// ����� 2 - LU ����������
	// http://ru.wikipedia.org/wiki/LU-����������
	// ����:
	// A = L * U
	// A * x = b
	// L * U * x = b
	// L * y = b
	// U * x = y
	// �.�. L � U ����������� �� ���� �������� ������ ������������
	x = A.lu().solve( b );
	// �.�. ������ ������ ��������� �� �� ������ ������� ������� x,
	// � �� ������

	// ������� ����������
	cout << "\n������� �:\n" << A << endl;
	cout << "\n������� b:\n" << b << endl;
	cout << "\n��������� ���� x:\n" << x << endl;

	ToFile("x.m", x);
	getchar();
}
