#include <iostream>
#include "Dense"
#include "Cholesky"
#include "Extensions"

using namespace Eigen;
using namespace std;

//////////////////////////////////////////////////////////////////////////
// 2. ������� ���� � ������������ ��������.
//////////////////////////////////////////////////////////////////////////
void main()
{
	// ��� ������� ����� ������������ ��������� ���������
	// http://en.wikipedia.org/wiki/Cholesky_decomposition
	// A = L * transpose(L)
	// ������ ���: http://eigen.tuxfamily.org/dox/classEigen_1_1LLT.html
	// ��������: A = LL*, then solving Ly = b for y by forward substitution, and finally solving L*x = y for x by back substitution.

	setlocale(LC_ALL, "Russian");

	// ������ �������
	MatrixXd A = FromFile("A.m");

	// ������ ������
	VectorXd b = FromFile("b.m");

	// ������ ���� ��������� ����� solve � LL ���������� �� ���������
	MatrixXd x = A.llt().solve(b);

	// ������� ����������
	cout << "\n������� �:\n" << A << endl;
	cout << "\n������ b:\n" << b << endl;
	cout << "\n��������� ���� x:\n" << x << endl;

	// ��������� � ����
	ToFile("x.m", x);
	
	getchar();
}